/***** MAC0110 - EP3 *****/
  Nome: Eduardo Sandalo Porto
  NUSP: 11796510

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.

        Esta probabilidade é dada por 1 - (PROBABILIDADE_LOBO + PROBABILIDADE_COELHO + PROBABILIDADE_COMIDA)

    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?

        Como o código está no momento, não há diferença em funcionamento do terreno e o terreno especial. As diferenças são: o primeiro tem símbolo 🌿 e o segundo 🍀; o primeiro tem chance de 0.99 de aparecer e o segundo 0.01

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?

        Um lobo precisará de 20 pontos de energia enquanto um coelho precisará de 12 pontos.

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?

        Nesta implementação, um animal só pode morrer através da função morre!() caso sua energia chegue a zero, assim, o valor de sua energia na matriz `energia` já será zero. O tratamento de um animal ser devorado é feito pela função ocupa_vizinho!()

/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?

        Usando os valores iniciais, na maioria das vezes os lobos comem todos ou quase todos os coelhos até por volta da iteração 30 e depois acabam morrendo de fome. Com mais iterações que isso todos os animais acabam morrendo e lentamente a comida (cenoura) volta a se regenerar

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Em uma ilha de tamanho 20 foi necessário diminuir muito a quantidade de lobos, pois se reproduzem muito rapidamente, e aumentar a frequência da regeneração da comida. Alterei a PROBABILIDADE_LOBO para 0.0001 e a REGENERACAO_TERRENO para 0.5

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        A partir de 30 iterações não é muito complicado levar a ilha a não ter mais animais. Para que isso aconteça antes é possível diminuir a quantidade de energia dada por uma comida (cenoura). Reduzindo ENERGIA_COMIDA a 1, na maioria das vezes não há mais animais já na iteração 30.

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?

        A função simula2() salva, a cada iteração da simulação, o estado desta em um DataFrame, enquanto a função simula() não salva a simulação.

    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

        A função desenha exatamente dois gráficos. O primeiro contém a quantidade de coelhos e lobos na simulação ao longo das iterações e o segundo contém a quantidade de comida e energia total presente na simulação ao longo das iterações.

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        Pude observar que muitas vezes sobrevivem alguns lobos após, por exemplo, a iteração 50, mas no geral o tabuleiro fica bem vazio com apenas comida regenerando.

    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        Percebi que, rodando em iterações até 50, a quantidade de coelhos, apesar de durar mais, ainda reduz a quase 0 ou 0. No entanto, com certeza houve melhoria na expectativa de vida dos coelhos.

    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        Como esperado, a quantidade de tempo que os animais sobrevivem diminuiu drasticamente. Até a iteração 30 já não havia mais animais em todas as vezes que testei. Os lobos conseguiram se manter com uma quantidade mais ou menos constante enquanto ainda haviam coelhos vivos, mas após isso gradativamente somem.
